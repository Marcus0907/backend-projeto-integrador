const mongoose = require("mongoose");

const OngSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    mapsLocation:{
        type: String,
        required: true
    },
    image:{
        type: String,
        require: true
    },
    address:{
        type: String,
        required: true,
    },
    telephone:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
})

mongoose.model("Ong", OngSchema);