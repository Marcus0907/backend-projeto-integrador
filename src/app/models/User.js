const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const UserSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
    },

    email: {
        type: String,
        unique: true, //nao permite valor repitido
        required: true,
        lowercase: true,
    },

    password: {
        type: String,
        required: true,
        select: false // nao traz campo 
    },

    passwordResetToken: {
        type: String,
        select: false 
    },

    passwordResetExpires: {
        type: Date,
        select: false
    },

    createdAt: {
        type: Date,
        default: Date.now
    }
})

UserSchema.pre( 'save' ,async function(next) {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;
    return next();
})

const User = mongoose.model("User", UserSchema);

module.exports = User; 