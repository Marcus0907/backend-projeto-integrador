const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
const User = mongoose.model("User");
const path = require('path');

const authConfig = require("../../config/auth.json");
const mailer = require("../../modules/mailer");
/* const { reset } = require("nodemon"); */

function generateToken(userId){
    return jwt.sign({ id: userId }, authConfig.secret , {expiresIn: 86400}); //info dos tokens, bom alterar, só teste
}

module.exports = {
    async cadastrar(req, res){
        const email = req.body.email;
        try{
            
            if(await User.findOne({email}))
                return res.status(400).send({error:"Usuário já cadastrado"}); 


            const user = await User.create(req.body);

            user.password = undefined;

            const token = generateToken(user.id);

            return res.json({user, token: token});
        }
        catch (err){
            return res.status(400).send({error: 'Erro ao cadastrar'})
        }
    },

    async login(req,res){

        const { email, password } = req.body;

        const user = await User.findOne({ email }).select("+password");

        if(!user){
            return res.status(400).send({erro : "Usuário não encontrado"});
        }

        if(!await bcrypt.compare(password, user.password)){
            return res.status(400).send({ erro : "Senha incorreta"});
        }

        user.password = undefined;
        
        const token = generateToken(user.id);
        
        return res.status(200).send({ user, token: token});




    },

    async forgotPassword(req, res){

        const email = req.body.email;
        
        try{
            const user = await User.findOne({ email });

            if(!user){
                return res.status(400).send({erro : "Usuário não encontrado"});
            }

            const token = crypto.randomBytes(20).toString('hex');

            const now = new Date();
            now.setHours(now.getHours() + 1 );

            await User.findByIdAndUpdate(user.id,{ 
                '$set': {
                    passwordResetToken: token,
                    passwordResetExpires: now
                }
             });

             mailer.sendMail({
                 to: email,
                 from: 'mvleite0908@gmail.com',
                 template: 'auth/forgot-password',
                 context: { token }
             }, (err) => {
                 console.log(err);
                if(err)
                    res.status(400).send({err: 'Erro ao enviar email'})

                return res.send();    
                    
             })

        }catch(err){
            console.log(err);
        }
        


    },

    async resetPassword(req,res){

        const { email, token, password } = req.body;

        try{

        }catch(err){
            res.status(400).send({err:'Erro ao resetar senha'});
        }

        const user = await User.findOne({ email }).
            select('+passwordResetToken passwordResetExpires')

        if(!user)
            return res.status(400).send({err:'Usuário não encontrado'});
            
        if(token !== user.passwordResetToken)    
            return res.status(400).send({err:'Token incorreto'});
        
        const now = new Date();
        
        if(now > user.passwordResetExpires)
            return res.status(400).send({err:'Token expirou'});

        user.password = password;    

        await user.save();


        res.send({mensagem:'Senha atualizada com sucesso'})

    },

    async getUsers(req, res){
        const users = await User.find();
        return res.json(users);
    },

    async deleteUser(req, res){
        await User.findByIdAndRemove(req.params.id);
        return res.send({mensagem: "usuário deletado"});
    }
}
