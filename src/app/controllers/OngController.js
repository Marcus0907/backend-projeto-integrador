const mongoose = require("mongoose");
const Ong = mongoose.model("Ong");
const path = require("path")

const fs = require('fs');
//mudar diretorio
const dir = '/Users/patricialeite/Desktop/marcus/projetoIntegrador/backend/backend-projeto-integrador/upload';


module.exports = {
    async createOng(req, res){
        try{
            const ong = await Ong.create(req.body);
            return res.json(ong);
        }catch(err){
            return res.send({erro: 'Não foi possível criar', log: err})
        }
    },

    async updateOng(req,res){
        try{
            const ong = await Ong.findByIdAndUpdate(req.params.id, req.body, {new:true} );
            return res.json(ong);
        }catch(err){
            return res.send({erro: 'Não foi possível atualizar', log: err})
        }
        
    },

    async getOng(req, res){
        try{
            const ong = await Ong.findById(req.params.id);
            return res.json(ong);
        }catch(err){
            return res.send({erro: 'Não foi possível pegar essa ong', log: err})
        }
        
    },

    async getAllOngs(req, res){
        try{
            const ongs = await Ong.find();
            return res.json(ongs);
        }catch(err){
            return res.send({erro: 'Não foi possível pegar as ongs', log: err})
        }
        
    },

    async deleteOng(req,res){
        try{
            await Ong.findByIdAndRemove(req.params.id);
            return res.send({mensagem: 'Ong deletada com sucesso'})
        }catch(err){
            return res.send({erro: 'Não foi possível deletar', log: err})
        }
        
    },

    async getImage(req, res){
        if(req.body.path)
            res.sendFile(path.join(dir, ('/' +req.body.path)));
        else
            res.send({err:'Erro ao pegar a imagem'})    
    }
}