const express = require("express");
const routes = express.Router();
const authMiddleware = require("./app/middlewares/auth");





//users
const UserController = require("./app/controllers/UserController");

routes.post("/register", UserController.cadastrar);

routes.post("/login", UserController.login);

routes.post("/forgot-password", UserController.forgotPassword);

routes.post("/reset-password", UserController.resetPassword);

routes.use(authMiddleware); // ativando os middlewares

routes.get("/users", UserController.getUsers);

routes.delete("/user/:id", UserController.deleteUser);

//Ongs

const OngController = require("./app/controllers/OngController");

routes.post("/ong", OngController.createOng);

routes.put("/ong/:id", OngController.updateOng);

routes.get("/ong/:id", OngController.getOng);

routes.get("/ongs", OngController.getAllOngs);

routes.delete("/ong/:id", OngController.deleteOng);

routes.post("/ong-images", OngController.getImage);

//products

const ProductContoller = require("./app/controllers/ProductController");

routes.get("/products", ProductContoller.index);

routes.get("/products/:id", ProductContoller.show);

routes.put("/products/:id", ProductContoller.update);

routes.post("/products", ProductContoller.store);

routes.delete("/products/:id", ProductContoller.destroy);

module.exports = routes;