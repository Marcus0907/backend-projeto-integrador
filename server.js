const express = require("express"); 
const mongoose = require("mongoose");
const cors = require("cors");
const requireDir = require("require-dir");
require('dotenv/config');

const PORT = process.env.PORT;
const DB_URI = process.env.MONGODB_URI_PROD;



//iniciando app
const app = express();

//parar poder receber arquivos json
app.use(express.json());

//habilitando cors
app.use(cors());

// conectando com banco
mongoose.connect(DB_URI, { useNewUrlParser:true ,useUnifiedTopology: true, useFindAndModify: false});
//habilitando identificadores unicos
mongoose.set('useCreateIndex', true)

//modelos importartados automatico
requireDir("./src/app/models");

//habilitando ejs
app.set("view engine", "ejs");

//centralizando todas as rotas
app.use("/api", require("./src/routes"))

//rodando aplicaao na porta
app.listen(PORT, () => {
    console.log("listen on port "+ PORT + " "+ DB_URI );
})


const path = require("path")
const multer = require("multer");
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "upload/")
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname)
  },
})
const upload = multer({ storage })


//para testes
app.get("/uploadImage", (req,res) => {
  return res.render("index");
})

app.post("/uploadImage", upload.single('img'),(req,res) => {
  console.log(req.body, req.file);
  return res.sendFile(path.join(__dirname, req.file.path));
})