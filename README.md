# uma-mao-lava-outra

> A Express project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm run dev

# donwload mongo image with docker
docker pull mongo

# config docker 
docker run --name mongodb -p 2707:2707 -d mongo

# start docker
docker start mongodb